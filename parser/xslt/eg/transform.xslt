<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<html> 
<body>
  <h1>Region - Egypt</h1>
    <xsl:for-each select="region/section">
        <div>
            <h2>
                <xsl:value-of select="current()/@name"/>
            </h2>
            <xsl:for-each select="current()/subsection">
                
                <h3>
                    <xsl:value-of select="current()/@name"/>
                </h3>
                <p>
                    <xsl:value-of select="current()/data"/>
                </p>
            </xsl:for-each>    

        </div>
    </xsl:for-each>
</body>
</html>
</xsl:template>
</xsl:stylesheet>

