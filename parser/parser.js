var https = require('https');
var request = require('request');
var fs = require('fs');
var builder = require('xmlbuilder');

main();


function main() {
    var args = process.argv.slice(2);
    if (args.length !== 2) {
        console.log("Bad usage\nuse node index.js <htmlPathToFile> <regionName>");
        process.exit();
    } else if (!fs.existsSync(args[0])) {
        console.log("File " +  __dirname + args[0] + " was not found");
        process.exit();
    }

    fs.readFile( __dirname + "/" + args[0], function (err, data) {
        if (err) {
            throw err;
        }
        parseIt(data.toString(),args[1]);
    });
}
/*
function downloadPage() {
    var res = request('GET', 'http://example.com');
    console.log(res.getBody());

    parseIt(res.getBody());
    //request('https://www.cia.gov/library/publications/the-world-factbook/geos/print_ve.html', function (error, response, body) {
    request('https://www.cia.gov/library/publications/the-world-factbook/geos/print_it.html', function (error, response, body) {
        //console.log('error:', error); // Print the error if one occurred
        //console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
        parseIt(body);
        //console.log('body:', body); // Print the HTML for the Google homepage.
    });
}
*/
function parseIt(page,regionName) {
    var xml = builder.create("region").att("id",regionName).att("name",capitalizeFirstLetter(regionName));

    const cheerio = require('cheerio');
    const $ = cheerio.load(page);

    var list = $("li[id$='-section-anchor']");
    var listSection = $("li[id$='-section']");

    for (var i = 0; i < list.length; i++) {
        var node = list[i];
        var nodeSection = listSection[i];
        var mainHeader = $(node.children[1]).attr("sectiontitle");
        //console.log(mainHeader);

        var header = builder.create("section").att("name",mainHeader);


        var categories = $(nodeSection).children($(".category"));
        var fields = $(nodeSection).children($("div[id^='field']").not(".category"));

        for (var y = 0; y < fields.length; y++) {
            var catName = $(categories[y]).text().trim();
            var text = $(fields[y]).text().trim();

            try {
                var classname = $(categories[y]).attr("id");
                var id = $(fields[y]).attr("id");

                if (classname != null && id != null) {
                    //console.log("   "+classname);
                    //console.log("   "+prettyText(catName));
                    //console.log("        "+prettyText(text));
                    //console.log("        "+id);
                    //console.log("_");

                    var sub = builder.create("subsection").att("name",prettyText(catName));
                    var data = builder.create("data").txt(prettyText(text));
                    sub.importDocument(data);
                    header.importDocument(sub);
                }
            } catch (e) {
                console.log("--------------------------");
            }


        }

        xml.importDocument(header);
        //section($,node,arr);
    }
    console.log(xml.end({pretty: true}));

}

function prettyText(text) {
    return text.replace(/\n/g, '').replace(/  +/g, ' ').trim();
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}