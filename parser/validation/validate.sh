
#validace samotneho xml
xmllint --valid ../xml/eg.xml --noout
xmllint --valid ../xml/ve.xml --noout
xmllint --valid ../xml/gr.xml --noout
xmllint --valid ../xml/it.xml --noout

#validace proti dtd
xmllint --dtdvalid validation.dtd ../xml/eg.xml  --noout
xmllint --dtdvalid validation.dtd ../xml/ve.xml  --noout
xmllint --dtdvalid validation.dtd ../xml/gr.xml  --noout
xmllint --dtdvalid validation.dtd ../xml/it.xml  --noout

#validace proti relaxng schematu
xmllint --relaxng validation.rng ../xml/eg.xml --noout
xmllint --relaxng validation.rng ../xml/ve.xml --noout
xmllint --relaxng validation.rng ../xml/gr.xml --noout
xmllint --relaxng validation.rng ../xml/it.xml --noout
