﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:output method="xml" indent="yes"/>
    
<xsl:template match="/">
   <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<fo:layout-master-set>
            <fo:simple-page-master master-name="my-page"
                                   page-height="297mm"
				   page-width="210mm"
				   margin="2cm">
		<fo:region-body margin-bottom="10mm"/>
		<fo:region-after extent="10mm"/>
            </fo:simple-page-master>
	</fo:layout-master-set>
	<fo:page-sequence master-reference="my-page">            
            <fo:flow flow-name="xsl-region-body">					
                <xsl:apply-templates/>                                        
            </fo:flow>
	</fo:page-sequence>
    </fo:root>    
</xsl:template>


<xsl:template match="region">
    <fo:block break-after="page">
        <fo:block font-weight="700" font-size="280%" margin-bottom="10mm" color="#191fd9">
            <xsl:value-of select="@name" />
            <fo:block margin-left="6pt" margin-bottom="2pt">
              <fo:external-graphic src="url(italy.jpg)" 
                                   content-width="2cm"/>
            </fo:block>

        </fo:block>
        <xsl:apply-templates select="section"/>
    </fo:block>
</xsl:template>

<xsl:template match="section">
    <fo:block margin-bottom="10mm">
        <fo:block font-weight="700" font-size="200%" margin-bottom="5mm" color="#191fd9">
            <xsl:value-of select="@name"/>
        </fo:block>        
        <xsl:apply-templates select="subsection"/>
    </fo:block>        
</xsl:template>


<xsl:template match="subsection">
    <fo:block margin-left="5mm">
        <fo:block font-weight="700" font-size="150%" margin-bottom="5mm" margin-top="5mm" color="#1f41ff">            


            <xsl:value-of select="@name"/>
            <fo:block font-weight="400" font-size="70%" margin-bottom="5mm" margin-top="5mm" color="black">            
              <xsl:value-of select="data"/>
            </fo:block>   
        </fo:block>   
        <xsl:apply-templates select="data"/>
    </fo:block>
</xsl:template>


<xsl:template match="data">
    <fo:block margin-bottom="2mm" margin-left="10mm">
        <fo:block font-weight="700" font-size="90%" color="#000000">    
                <xsl:value-of select="@name"/>
    </fo:block>
    </fo:block>
</xsl:template>

        
</xsl:stylesheet>